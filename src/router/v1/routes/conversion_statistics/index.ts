import { Router } from 'express'

import convertRoute from './routes/conversion_statistics'

const router = Router()

router.use(convertRoute)

export default router
