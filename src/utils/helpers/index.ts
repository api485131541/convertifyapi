import { FileExtension } from '@type/file'

import path from 'path'

export const extractFileExtension = (filePath: string): FileExtension => path.extname(filePath).slice(1) as FileExtension
