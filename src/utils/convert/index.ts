import { extractFileExtension } from '@utils/helpers'

import { FileExtension, ImageExtension } from '@type/file'

import sharp from 'sharp'

const convertImageBuffer = async (buffer: Buffer, targetExtension: `${ImageExtension}`, outputFilePath: string): Promise<string> => {
  return new Promise((resolve, reject) => {
    sharp(buffer)
      .toFormat(targetExtension)
      .toFile(outputFilePath)
      .then(() => resolve(outputFilePath))
      .catch(err => reject(err))
  })
}

const converters: { [key: string]: (buffer: Buffer, targetExtension: any, outputFilePath: string) => Promise<string> } = {
  image: convertImageBuffer,
}

const extensionCheckers: { [key: string]: (ext: FileExtension) => boolean } = {
  image: ext => Object.values(ImageExtension).includes(ext as ImageExtension),
}

export const convertFile = async (buffer: Buffer, extension: FileExtension, targetExtension: FileExtension, outputFilePath: string): Promise<string> => {
  for (const type of Object.keys(extensionCheckers)) {
    if (extensionCheckers[type](extension) && extensionCheckers[type](targetExtension)) {
      return converters[type](buffer, targetExtension, outputFilePath)
    }
  }

  throw new Error('Unsupported file type for conversion or invalid target extension provided.')
}
