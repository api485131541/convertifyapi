import { convertFile } from '@utils/convert'
import { extractFileExtension } from '@utils/helpers'

import { prisma } from '@/app'

import { Request, Response } from 'express'
import fs from 'fs'
import path from 'path'

export const convertController = {
  convertFile: async (req: Request, res: Response) => {
    const targetExtension = req.body.targetExtension

    if (!req.file) return res.status(400).send({ message: 'No file uploaded.' })

    const buffer = req.file.buffer
    const extension = extractFileExtension(req.file.originalname)
    const outputFilePath = path.resolve('uploads', `${Date.now()}-converted.${targetExtension}`)

    try {
      const convertedFilePath = await convertFile(buffer, extension, targetExtension, outputFilePath)
      res.sendFile(convertedFilePath, async () => {
        fs.unlinkSync(convertedFilePath)
        await prisma.conversion_statistics.create({
          data: {
            file_size: req.file!.size,
            source_extension: extension,
            target_extension: targetExtension,
            created_at: new Date().toISOString(),
            name: req.file!.originalname,
          },
        })
      })
    } catch (error) {
      res.status(500).send({ message: `Error during file conversion : ${error}` })
    }
  },
}
