import { Router } from 'express'
import multer from 'multer'

import { convertController } from '../controller'

const router = Router()

//TODO - Implement the multer storage and upload in another file for better separation of concerns
const upload = multer({ storage: multer.memoryStorage() })

router.post('/', upload.single('file'), convertController.convertFile)

export default router
