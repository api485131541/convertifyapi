import { Router } from 'express'

import convertRoute from './routes/convert'

const router = Router()

router.use(convertRoute)

export default router
