# FileConvertWizard

🌟 FileConvertWizard is a PWA (Progressive Web App) using Nuxt.js to offer a file conversion service. This application allows converting various file types (PDF, DOCX, TXT, JPEG, PNG, MP3, WAV) with advanced features such as internal storage, notifications, and more.

## About

📝 FileConvertWizard is developed with Nuxt.js for the front-end and a PHP backend for handling file conversions. The application provides REST endpoints for managing conversions.

## Prerequisites

🔧 Before running the application, make sure you have the following installed:

- Node.js (version 20.9.4)
- npm (version 10.1.0)
- MariaDB

You'll also need to set up a `.env` file with the necessary environment variables. Rename the `.env-sample` file to `.env` and fill in the required information, including database connection details. The SQL file for the database is provided, and the connection information is stored in the `.env` file.

## Installation

⚙️ Clone the GitHub repository:

```bash
git clone https://gitlab.com/api485131541/convertifyapi.git
```

## Usage

📦 Install dependencies:

```bash
npm install
```

🚀 To start the application in development mode:

```bash
npm run dev
```

🌐 The application by default runs on port 3000.

## Information

### Future Versions

🔜 In future versions, there will be:

- Authentication and authorization using JWT tokens.
- Storage of the conversion history in the database instead of client-side localStorage.
- Addition of an API documentation.
- New routes for enhanced functionality.

## Contributing

🤝 As of now, there are no contributors to this project.

## Technologies Used

🛠️ - Node.js (version 20.9.0)
🚀 - Express (version 4.19.2)
💻 - TypeScript (version 5.4.5)
🔒 - Prisma (version 5.15.0)
