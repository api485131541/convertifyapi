import { prisma } from '@/app'

import { Request, Response } from 'express'

export const conversionStatisticsController = {
  getStatistics: async (_: Request, res: Response) => {
    try {
      const statistics = await prisma.conversion_statistics.findMany()
      return res.json(statistics)
    } catch (error) {
      return res.status(500).json({ message: error })
    }
  },
}
