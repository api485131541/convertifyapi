import { Router } from 'express'

import '../controller'
import { conversionStatisticsController } from '../controller'

const router = Router()

router.get('/', conversionStatisticsController.getStatistics)

export default router
