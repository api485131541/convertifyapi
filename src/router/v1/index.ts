import { Router } from 'express'

import conversionStatisticsRouter from './routes/conversion_statistics'
import convertRouter from './routes/convert'

const router = Router()

router.use('/conversion_statistics', conversionStatisticsRouter)
router.use('/convert', convertRouter)

export default router
